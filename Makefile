# Cryptizer makefile for GNU gcc

C_DIR    = .
C_OUT    = .
C_LIB    = -lpthread
C_DFLAGS = -O0 -ggdb
C_FLAGS  = -O3
C_DEBUG  = -D_DEBUG
C_DBGTST = $(C_DEBUG) -DCRYPT_TEST -DCRYPT_INLINE_TEST
C_NAME   = cryptizer
C_SRC    = crypt.cpp debug.cpp main.cpp

all:
	g++ $(C_SRC) $(C_FLAGS) $(C_LIB) -o $(C_OUT)/$(C_NAME)
	strip $(C_OUT)/$(C_NAME)

debug:
	g++ $(C_SRC) $(C_DEBUG) $(C_DFLAGS) $(C_LIB) -o $(C_OUT)/$(C_NAME)

test:
	g++ $(C_SRC) $(C_DBGTST) $(C_DFLAGS) $(C_LIB) -o $(C_OUT)/$(C_NAME)

clean:
	rm $(C_OUT)/$(C_NAME)