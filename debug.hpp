/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

/**
* @file debug.hpp
* @author Damian Dorosz
* @date 25 Nov 2019
* @brief Cryptizer main debug header.
*
* General purpose of this is for debugging and testing.
*/

#ifndef _DEBUG_H
#define _DEBUG_H

#ifdef _DEBUG

#include <assert.h>

#if defined _WIN32 || defined _WIN64
#include <Windows.h>
#endif 

#ifdef _MSC_VER
// #include <debugapi.h> // included in Windows.h
#else
// #include <w32api\debugapi.h>
#include <iostream>
#endif

#include <string>

#define DEBUG_EQUAL_ASSERT(resultValue, expectedValue, message) \
	if(!debug::debugTestResult(resultValue, expectedValue)) \
		debug::debugAssert(message)

#define DEBUG_NOT_EQUAL_ASSERT(resultValue, expectedValue, message) \
	if(debug::debugTestResult(resultValue, expectedValue)) \
		debug::debugAssert(message)

using namespace std;

/**
*  @brief debug - debug.hpp debug.cpp
*  Debugging features class.
*/
class debug {
public:
	/** @brief debugAssert - print message in output window/console
	*
	* @param stringMessage string - message to print
	*
	* @return void
	*
	*/
	static void debugAssert(string stringMessage);

	/** @brief debugTestResult - print message in output window/console
	*
	* @param testResult <T> - testing result value
	*
	* @param expectedResult <T> - expected result value
	*
	* @return bool - "true" if both values are euqal, "false" if not
	*
	*/
	template <class T>
	inline static bool debugTestResult(T testResult, T expectedResult) {
		return testResult == expectedResult;
	}

#ifdef CRYPT_TEST

	/** @brief debugTestResult - print message in output window/console
	*
	* @param testResult <T> - testing result value
	*
	* @param expectedResult <T> - expected result value
	*
	* @return bool - "true" if both values are euqal, "false" if not
	*
	*/
	static void* debugTestRandomizeData(void* data, size_t dataLength);

	/** @brief debugTestResult - print message in output window/console
	*
	* @param testResult <T> - testing result value
	*
	* @param expectedResult <T> - expected result value
	*
	* @return bool - "true" if both values are euqal, "false" if not
	*
	*/
	static bool debugTestCryptData(void* data, void *cryptData, size_t dataLength);

#endif

private:
#ifdef CRYPT_TEST

	/** @brief debugTestCryptAlgorithm - the same like _cryptinfo::cryptAlgorithm
	* but with known result
	*
	* @param dataValue char - current part/indexed data value
	*
	* @return char - crypted data value
	*
	*/
	static char debugTestCryptAlgorithm(char dataValue);

#endif
};

#endif /* _DEBUG */

#endif /* _DEBUG_H */

