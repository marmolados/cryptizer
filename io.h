/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

/**
* @file io.h
* @author Damian Dorosz
* @date 25 Nov 2019
* @brief Cryptizer main/io header.
*
* Here are all functionalities that do not apply to cryptography.
*/

#ifndef _IO_H
#define _IO_H

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <string.h>

#ifdef _WIN32

#include <io.h> 
#define access    _access_s
#define strcmpi   _strcmpi

#ifdef _MSC_VER

#define printf    printf_s

#ifndef inline
#define inline    __inline // inline works only for >= 1900
#endif

#endif

#else

#include <ctype.h>

#ifndef strcmpi

inline int strcmpi(const char * _Str1, const char * _Str2) {
	long long str1len = strlen(_Str1), str2len = strlen(_Str2);
	char * str1 = (char*)calloc(1, str1len + 1), *str2 = (char*)calloc(1, str2len + 1);
	if (!str1 || !str2) {
		if (str1)
			free(str1);
		return -1;
	}

	for (register long long i = 0; i < str1len; i++) // there is some improvement here ;-)
		str1[i] = tolower(_Str1[i]);

	for (register long long i = 0; i < str2len; i++)
		str2[i] = tolower(_Str2[i]);

	int ret = strcmp(str1, str2);

	free(str1);
	free(str2);

	return ret;
}

#endif

#include <unistd.h>

#endif

/*
	TODO:
	- change FILE * type to handle_file type
	- separate os functions
*/

/** @brief FileMode - openFile mode
*
* UnknownMode - cannot processing file by this!
* ReadMode - file is only for reading prepared
* WriteMode - file is only for writting prepared
* AppendMode - file appending mode (TODO!)
*
*/
enum FileMode {
	UnknownMode = 0,
	ReadMode	= (1 << 0),
	WriteMode	= (1 << 1),
	AppendMode	= (1 << 2),
};

using namespace std;

/** @brief isValidPathName - check is current file path is valid
*
* @param path string - full file path name
*
* @return bool - "true" if path is valid or "false" if not
*
*/
inline bool isValidPathName(string path) { // probably it will be obsolete for openFile
	// TODO!
	return true;
}

/** @brief getInputFileName - get input file path
*
* @param argCount int - console parameters count
*
* @param argValues const char ** - console parameters for parsing
*
* @return string - input file path
*
*/
inline string getInputFileName(int argCount, const char **argValues) {
	if (argCount < 2)
		return ""; // string shall not be nullptr!

	string inputFileName = argValues[1];
	if(access(inputFileName.c_str(), 0) || !isValidPathName(inputFileName))
		return "";

	return inputFileName;
}

/** @brief getOutputFileName - get output file path
*
* @param argCount int - console parameters count
*
* @param argValues const char ** - console parameters for parsing
*
* @return string - output file path
*
*/
inline string getOutputFileName(int argCount, const char **argValues) {
	if (argCount < 3)
		return "result.bin";

	string outputFileName = argValues[2];
	if (!isValidPathName(outputFileName))
		return string("result.bin");

	return outputFileName;
}

/** @brief getFile - get handle to file
*
* @param pathName const string/char* - file localization/path
*
* @param fileMode FileMode - set how to file should be processing:
* ReadMode - file is only for reading prepared
* WriteMode - file is only for writting prepared
* AppendMode - file appending mode (TODO!)
*
* @return fileHandle FILE * - current file handle
*
*/
inline FILE *getFile(const char* pathName, FileMode fileMode) {
	switch (fileMode)
	{
	case ReadMode:
		return fopen(pathName, "rb");
	case WriteMode:
		return fopen(pathName, "wb");
	case (ReadMode | WriteMode):
		return fopen(pathName, "wb+");
	case AppendMode: // TODO!
	case UnknownMode:
	default:
		break;
	}

	return nullptr;
}

inline FILE *getFile(const string pathName, FileMode fileMode) {
	return getFile(pathName.c_str(), fileMode);
}

/** @brief getFileSize - geting full data size from file
*
* @param fileHandle FILE * - current file handle
*
* @param fileOffeset size_t - set file offset (default is 0)
*
* @return size_t - file data size
*
*/
inline size_t getFileSize(FILE * fileHandle, size_t fileOffeset = 0) {
	fseek(fileHandle, 0, SEEK_END);
	long fileSize = ftell(fileHandle); // cannot open file above 2GB data! (it's limit)
	if (!fileOffeset)
		rewind(fileHandle); // cannot fseek set to 0 for read! (bug?)
	else fseek(fileHandle, fileSize, SEEK_SET);

	return fileSize;
}

/** @brief writeFile - storing file data to destination memory pointer
*
* @param fileHandle FILE * - current file handle
*
* @param dataToWrite void * - destination memory pointer to write
*
* @param dataSize size_t - data amount to write
*
* @return bool - "true" if it was stored or "false" if not
*
*/
inline bool writeFile(FILE * fileHandle, const void * dataToWrite, size_t dataSize) {
	if (!fileHandle)
		return false;

	return (fwrite(dataToWrite, 1, dataSize, fileHandle) == dataSize);
}

/** @brief readFile - reading file data to destination memory pointer
*
* @param fileHandle FILE * - current file handle
*
* @param dataToRead void * - destination memory pointer to read
*
* @param dataSize size_t - data amount to read
*
* @return bool - "true" if it was read or "false" if not 
*
*/
inline bool readFile(FILE * fileHandle, void * dataToRead, size_t dataSize) {
	if (!fileHandle)
		return false;

	return (fread(dataToRead, 1, dataSize, fileHandle) == dataSize);
}

/** @brief flushFile - storing all cached data immediately
*
* @param fileHandle FILE * - current file handle
*
* @return void
*
*/
inline void flushFile(FILE * &fileHandle) {
	if (!fileHandle)
		return;

	fflush(fileHandle);
}

/** @brief closeFile - closing current file handle
*
* @param fileHandle FILE * - current file handle
*
* @return void
*
*/
inline void closeFile(FILE * &fileHandle) {
	if (!fileHandle)
		return;

	fclose(fileHandle);
	fileHandle = 0;
}

// -------------- TODO: separate this part for os.h/cpp -----------------------------

/** @brief printMessage - is printing ready parsed message
*
* @param message string - message to print
*
* @return void
*
*/
inline void printMessage(string message) {
	printf("%s", message.c_str());
}


/** @brief copyMemory - copying memory
*
* @param dstMemoryPointer void * - destination memory pointer
*
* @param srcMemoryPointer void * - source memory pointer
*
* @param memorySize size_t - memory amount
*
* @return void
*
*/
inline void copyMemory(void * dstMemoryPointer, void * srcMemoryPointer, size_t memorySize) {
	memcpy(dstMemoryPointer, srcMemoryPointer, memorySize); // TODO: should be return memcpy(...)
}


/** @brief allocMemory - allocating memory
*
* @param memorySize size_t - memory amount
*
* @return void * - memory pointer
*
*/
inline void * allocMemory(size_t memorySize) { // TODO: malloc & calloc separate
	return calloc(1, memorySize);
}

/** @brief clearMemory - clean allocated memory
*
* @param memoryHandle void * - pointer to memory
*
* @param memorySize size_t - memory amount to clear
*
* @return void
*
*/
inline void clearMemory(void * memoryHandle, size_t memorySize) {
	memset(memoryHandle, 0, memorySize);
}

/** @brief freeMemory - freeing allocated memory
*
* @param memoryHandle <T> - pointer to free memory
*                
* @return void
*
*/
template <class T>
inline void freeMemory(T &memoryHandle) {
	if(memoryHandle)
		free(memoryHandle);
	memoryHandle = 0;
}

#ifdef CRYPT_INLINE_TEST

// -------------- TODO: separate this part for strutil.h/cpp -------------------------

enum StringCompareMode {
	Standard = 0,
	CaseSensitive = 1,
};

/** @brief stringCompare - special string comparer function
*
* @param comparedString const string/char* - first string to compare
*
* @param compareString const string/char*  - second string to compare
*
* @return bool - "true" if string is equal, "false if not
*
*/
inline bool stringCompare(const char * comparedString, const char * compareString, StringCompareMode stringCompareMode = Standard) {
	switch (stringCompareMode)
	{
	case CaseSensitive:
		return !strcmp(comparedString, compareString);
	case Standard:
	default:
		return !strcmpi(comparedString, compareString);
	}
}

// the same for const string
inline bool stringCompare(const string comparedString, const string compareString, StringCompareMode stringCompareMode = Standard) {
	return stringCompare(comparedString.c_str(), compareString.c_str(), stringCompareMode);
}

#endif /* CRYPT_INLINE_TEST */

#endif /* _IO_H */