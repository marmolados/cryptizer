/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

/**
* @file info.h
* @author Damian Dorosz
* @date 25 Nov 2019
* @brief Cryptizer all required messages
*
* General purpose of this is easy editable all Cryptizer messages.
*/

#ifndef _INFO_H
#define _INFO_H

/*
	TODO: change macros for poedit format!
*/

#define CRYPTIZER_VER "0.0.0.1"
#define CRYPTIZER_NAME "Cryptizer"
#define CRYPTIZER_AUTHOR "Damian Dorosz"
#define CRYPTIZER_LICENSE "Copyright (C) 2019 " CRYPTIZER_AUTHOR ". <https://fsf.org/>\n" \
	"Everyone is permitted to copy and distribute verbatim copies\n" \
	"of this license document, but changing it is not allowed.\n\n"
#define CRYPTIZER_INFO CRYPTIZER_NAME " version " CRYPTIZER_VER " by " CRYPTIZER_AUTHOR ".\n\n" CRYPTIZER_LICENSE
#define CRYPTIZER_HELP CRYPTIZER_NAME " usage:\n\t" CRYPTIZER_NAME " <input file path> [<output file path>]\n" \
	"Example:\n\t" CRYPTIZER_NAME " crypted.bin uncrypted.png\n\tWarning! Cannot open file size above 2GB! (limit)\n"
#define CRYPTIZER_INPUT_ERROR "Cannot open input file!\n\n" CRYPTIZER_HELP
#define CRYPTIZER_MEMORY_ERROR "Cannot allocate required memory!\n"
#define CRYPTIZER_READ_ERROR "Cannot read input file!\n"
#define CRYPTIZER_WRITE_ERROR "Cannot write into output file!\n"
#define CRYPTIZER_ERROR " Error!\n"

#define CRYPTIZER_READ_PROCESSING "File reading... "
#define CRYPTIZER_CRYPT_PROCESSING "File crypting... "

#define CRYPTIZER_SUCCESS " Done!\n" // [obsolete!] " OK!\n"

#endif /* _INFO_H */