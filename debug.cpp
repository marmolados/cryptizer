/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

#ifdef _DEBUG

#include "debug.hpp"

void debug::debugAssert(string stringMessage)
{
#ifdef _MSC_VER
	if (IsDebuggerPresent())
		OutputDebugStringA(stringMessage.c_str());
#else
	cout << stringMessage.c_str() << endl;
#endif
}

#ifdef CRYPT_TEST

void * debug::debugTestRandomizeData(void * data, size_t dataLength)
{
	char* _data = (char*)data; // safe (int* -> is not safe) 
	for (register size_t i = 0; i < dataLength; i++)
		_data[i] = rand();

	return data;
}

bool debug::debugTestCryptData(void * data, void * cryptData, size_t dataLength)
{
	char* _data = (char*)data,
		*_cdata = (char*)cryptData;
	for (register size_t i = 0; i < dataLength; i++)
		if (debugTestCryptAlgorithm(_data[i]) != _cdata[i])
			return false;

	return true;
}

char debug::debugTestCryptAlgorithm(char dataValue)
{
	return ~dataValue;
}

#endif /* CRYPT_TEST */

#endif /* _DEBUG */

