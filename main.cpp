/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

#include "crypt.h"
#include "io.h"
#include "info.h"

#ifdef _DEBUG

#include "debug.hpp"

#endif

/*
	TODO:
	- filename change to pathname
	- set global file handle type
	- separate tests for test.cpp/h
*/

// this not requirement any comment (I guess?)
int main(int argCount, const char **argValues) {
#ifdef CRYPT_TEST
	/* General the Crypt_Test assumption is functionality check */
	cryptic c, c2;

	DEBUG_EQUAL_ASSERT(c.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be nullable)!\n");
	DEBUG_EQUAL_ASSERT(c.getCryptDataSize(), (size_t)0, "Wrong cryptic data size (should be 0)!\n");

	assert((c.getCryptData() == (cryptDataType)0));

	c2.setCryptData(allocMemory(100), 100);

	DEBUG_NOT_EQUAL_ASSERT(c2.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be not nullable)!\n");
	DEBUG_EQUAL_ASSERT(c2.getCryptDataSize(), (size_t)100, "Wrong cryptic data size (should be 100)!\n");

	assert((c2.getCryptData() != (cryptDataType)0));
	assert((c2.getCryptDataSize() == (size_t)100));

	c2.setCryptData(200);

	DEBUG_NOT_EQUAL_ASSERT(c2.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be not nullable)!\n");
	DEBUG_EQUAL_ASSERT(c2.getCryptDataSize(), (size_t)200, "Wrong cryptic data size (should be 200)!\n");

	assert((c2.getCryptData() != (cryptDataType)0));
	assert((c2.getCryptDataSize() == (size_t)200));

	cryptDataType * testVoidMemory = (cryptDataType *)malloc(100);
	cryptic c3(testVoidMemory, 100);
	
	DEBUG_NOT_EQUAL_ASSERT(c3.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be not nullable)!\n");
	DEBUG_EQUAL_ASSERT(c3.getCryptDataSize(), (size_t)100, "Wrong cryptic data size (should be 100)!\n");
	
	assert((c3.getCryptData() != (cryptDataType)0));

	c3.~cryptic();

	DEBUG_EQUAL_ASSERT(c3.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be nullable)!\n");
	DEBUG_EQUAL_ASSERT(c3.getCryptDataSize(), (size_t)0, "Wrong cryptic data size (should be 0)!\n");

	assert(!c3.getCryptData());

	cryptic c4(100);

	DEBUG_NOT_EQUAL_ASSERT(c4.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be not nullable)!\n");
	DEBUG_EQUAL_ASSERT(c4.getCryptDataSize(), (size_t)100, "Wrong cryptic data size (should be 100)!\n");

	assert(c4.getCryptData());

	void* testPtr;
	c4.setCryptData((testPtr = debug::debugTestRandomizeData(c4.getCryptData(), c4.getCryptDataSize())), c4.getCryptDataSize());

	DEBUG_EQUAL_ASSERT(c4.getCryptData(), (cryptDataType)testPtr, "Wrong cryptic data (should be equal to testPtr)!\n");

	assert(c4.getCryptData() == (cryptDataType)testPtr);

	testPtr = allocMemory(c4.getCryptDataSize());
	
	DEBUG_NOT_EQUAL_ASSERT(testPtr, (void*)0, "Out of memory?\n");
	assert(testPtr);

	copyMemory(testPtr, c4.getCryptData(), c4.getCryptDataSize());

	c4.decrypt();

	assert(debug::debugTestCryptData(testPtr, c4.getCryptData(), c4.getCryptDataSize()));

	c4.~cryptic();

	DEBUG_EQUAL_ASSERT(c4.getCryptData(), (cryptDataType)0, "Wrong cryptic data (should be nullable)!\n");
	DEBUG_EQUAL_ASSERT(c4.getCryptDataSize(), (size_t)0, "Wrong cryptic data size (should be 0)!\n");

	assert(!c4.getCryptData());

	freeMemory(testPtr);
	assert(!testPtr);

#ifdef CRYPT_INLINE_TEST
	// main test
	//char testTemporaryData[260];
	char * testTemporaryData = (char*)allocMemory(260);
	//clearMemory(testTemporaryData, sizeof(testTemporaryData));
	assert(!testTemporaryData[0]); // we assume that it works for all memory
	void * testData = allocMemory(100);
	assert(testData);
	freeMemory(testData);
	assert(!testData);
	string testFilePath = "testFile.bin", testFileData = "Testing data to write!\n",
		testCompare1Str = "test1String", testCompare2Str = "test1string";
	assert(stringCompare(testCompare1Str, testCompare2Str));
	assert(!stringCompare(testCompare1Str, testCompare2Str, CaseSensitive));

	// in/out test
	const char *testArgs[3] = { "1","main.cpp","3" };
	string testOutFileName = getOutputFileName(3, testArgs),
		testInFileName = getInputFileName(3, testArgs);
	assert(stringCompare(testInFileName, "main.cpp"));
	assert(stringCompare(testOutFileName, "3"));

	// io test
	FILE * testFileHandle = getFile(testFilePath, (FileMode)(WriteMode | ReadMode));
	assert(testFileHandle);
	assert(writeFile(testFileHandle, testFileData.c_str(), testFileData.size()));
	flushFile(testFileHandle); // required for reading! (if will not reading return 0)
	size_t testFileSize = getFileSize(testFileHandle);
	assert(testFileSize == testFileData.size());
	assert(readFile(testFileHandle, testTemporaryData, testFileSize));
	assert(stringCompare(testFileData, string(testTemporaryData), CaseSensitive));
	closeFile(testFileHandle);
	assert(!testFileHandle);

	freeMemory(testTemporaryData);
	assert(!testTemporaryData);

#endif /* CRYPT_INLINE_TEST */

#else /* Main program code */
	printMessage(CRYPTIZER_INFO);
	printMessage(CRYPTIZER_READ_PROCESSING);

	string inputFileName = getInputFileName(argCount, argValues),
		outputFileName;
	if (inputFileName.empty()) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_INPUT_ERROR);
		return 0;
	}
	outputFileName = getOutputFileName(argCount, argValues); // we certainty know this isn't empty!
	FILE * inputFileHandle = getFile(inputFileName, FileMode::ReadMode),
		*outputFileHandle;
	if (!inputFileHandle) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_INPUT_ERROR); // TODO: parsing error information
		return 0;
	}
	size_t inputFileSize = getFileSize(inputFileHandle);
	if(!inputFileSize) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_INPUT_ERROR); // TODO: inform that file is invalid!
		return 0;
	}

	// init crypter 
	cryptic cryptizer;
	cryptizer.setCryptData(inputFileSize);

	if (!cryptizer.getCryptData()) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_MEMORY_ERROR);
		cryptizer.~cryptic();
		closeFile(inputFileHandle);
		return -1;
	}
	
	// [obsolete!]
	/*cryptDataType cryptizerData = (cryptDataType)allocMemory(inputFileSize);
	if (!cryptizerData) {
		printMessage(CRYPTIZER_MEMORY_ERROR);
		cryptizer.~cryptic();
		return;
	}*/

	if (!readFile(inputFileHandle, cryptizer.getCryptData(), inputFileSize)) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_READ_ERROR);
		cryptizer.~cryptic();
		closeFile(inputFileHandle);
		return -1;
	}

	closeFile(inputFileHandle); // close input file is not requirement in this moment

	printMessage(CRYPTIZER_SUCCESS);
	printMessage(CRYPTIZER_CRYPT_PROCESSING);

	outputFileHandle = getFile(outputFileName, FileMode::WriteMode); // initalize output file
	if (!outputFileHandle) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_WRITE_ERROR);
		cryptizer.~cryptic();
		return -1;
	}

	// crypting & storing
	if (!writeFile(outputFileHandle, cryptizer.decrypt(), cryptizer.getCryptDataSize())) {
		printMessage(CRYPTIZER_ERROR);
		printMessage(CRYPTIZER_WRITE_ERROR); // TODO: more specific error information
		cryptizer.~cryptic();
		return -1;
	}

	closeFile(outputFileHandle);
	cryptizer.~cryptic(); // free cryptizer data

	// [obsolete!]
	// freeMemory(cryptizerData); // not required! cryptic destructor done it

	printMessage(CRYPTIZER_SUCCESS);

#endif /* CRYPT_TEST */

	return 0;
}