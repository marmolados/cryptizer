/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

#include "crypt.h"
#include "io.h"

// this give us certainty that data will be properly initialized!
cryptic::cryptic() {
	this->cryptData = (cryptDataType)0;
	this->cryptDataSize = 0;
}

cryptic::cryptic(void *cryptData, size_t cryptDataSize) {
	this->cryptData = (cryptDataType)cryptData;
	this->cryptDataSize = cryptDataSize;
}

cryptic::cryptic(size_t cryptDataSize) {
	this->cryptData = (cryptDataType)allocMemory(cryptDataSize); // [obsolete!] (cryptDataType)calloc(1, cryptDataSize);
	this->cryptDataSize = cryptDataSize;
}

cryptic::~cryptic() // disposing cryptic data
{
	if (this->cryptData) {
		freeMemory(this->cryptData);
		this->cryptData = (cryptDataType)0;
	}
	this->cryptDataSize = (size_t)0;
}

// in this case encryption and decryption is the same
/* cryptDataType cryptic::encrypt() {
	return this->decrypt();
} */

cryptDataType cryptic::decrypt() {
	if (!this->cryptData || !this->cryptDataSize)
		return 0; // no data according to requirements!
	// TODO: this should be configurable 
	size_t startIndex = (this->cryptDataSize >> 1); // two threads for now!

	// TODO: processing decision
	cryptProcessing(-((intptr_t)startIndex)); // in C there is no problem with big-endian so this is it

	return this->cryptData; // convenient solution for user
}

cryptDataType cryptic::getCryptData() {
	return this->cryptData;
}

size_t cryptic::getCryptDataSize() {
	return this->cryptDataSize;
}

void cryptic::setCryptData(size_t cryptDataSize) {
	this->cryptData = (cryptDataType)allocMemory(cryptDataSize);
	this->cryptDataSize = cryptDataSize;
}

/*void cryptic::setCryptData(cryptDataType cryptData, size_t cryptDataSize) {
	this->cryptData = cryptData;
	this->cryptDataSize = cryptDataSize;
}*/

void cryptic::cryptProcessing(intptr_t argValue) {
	/* if args tract as int <= 0  => dataIndex = 0,        dataSize = -argValue
							else  => dataIndex = argValue, dataSize = this->cryptDataSize
		for no args (args == 0)   => dataIndex = 0,		   dataSize = this->cryptDataSize */
	size_t currentCryptDataIndex = ((argValue <= 0) ? 0 : (size_t)argValue),
		currentCryptDataSize = ((argValue < 0) ? ((size_t)-argValue) : this->cryptDataSize);

	if (!currentCryptDataSize)
		return; // cannot processing zero data!

	cryptInfo currentCryptInfo, *currentThreadCryptInfo;
	currentCryptInfo.currentData = this->cryptData;
	currentCryptInfo.currentDataIndex = currentCryptDataIndex;
	currentCryptInfo.currentDataSize = currentCryptDataSize;

	if (argValue >= 0)
		cryptWork(&currentCryptInfo); // unused by now yet! 
	else { // TODO: this part should be moved to thread section
		vector<cryptInfo*> currentCryptInfos; // cryptic information list
		size_t currentThreadCount = (size_t)ceil(this->cryptDataSize / (double)currentCryptDataSize); // minimum threads
		for (register size_t i = 0; i < currentThreadCount; i++) {
			currentThreadCryptInfo = (cryptInfo*)allocMemory(sizeof(cryptInfo));
			if(i >= (MAX_THREADS - 1))
				currentCryptInfo.currentDataSize = this->cryptDataSize;
			copyMemory(currentThreadCryptInfo, &currentCryptInfo, sizeof(cryptInfo));
			currentCryptInfos.push_back(currentThreadCryptInfo);
			this->cryptThreads[i] = thread(cryptWork, (void*)currentThreadCryptInfo);
			if (i >= (MAX_THREADS - 1)) {
				currentThreadCount = MAX_THREADS;
				break;
			}
			currentCryptInfo.currentDataIndex += currentCryptDataSize;
			currentCryptInfo.currentDataSize += currentCryptDataSize;
			if (currentCryptInfo.currentDataSize > this->cryptDataSize)
				currentCryptInfo.currentDataSize = this->cryptDataSize;
		}

		for (register size_t i = 0; i < currentThreadCount; i++)
			if(this->cryptThreads[i].joinable()) // check if thread is able to join
				this->cryptThreads[i].join(); // wait for full processing

		for (register size_t i = 0; i < currentThreadCount; i++) {
			freeMemory(currentCryptInfos.at(i)); // free all thread infos
			this->cryptThreads[i].~thread(); // destroy all threads
		}

		currentCryptInfos.clear();
	}
}

void cryptWork(void * workArgs) {
	if (!workArgs) return;
	cryptInfo *currentCryptInfo = (cryptInfo*)workArgs;
	if (!currentCryptInfo->currentData || !currentCryptInfo->currentDataSize) return; // requirements
	for (register size_t i = currentCryptInfo->currentDataIndex; i < currentCryptInfo->currentDataSize; i++)
		if (!currentCryptInfo->cryptAlgorithm(i)) // crypting
			break;
}

bool _cryptinfo::cryptAlgorithm(size_t index) {
	if (index >= this->currentDataSize) /* "What for?" */
		return false;

	this->currentData[index] = ~this->currentData[index]; // simplest eg

	return true;
}

/*_cryptinfo::~_cryptinfo()
{
	if (this->currentData) {
		free(this->currentData);
		this->currentData = 0;
	}

	this->currentDataSize = 0;
}*/
