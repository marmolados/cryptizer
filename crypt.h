/*******************************************************************************
 * Copyright (C) 2019 by Damian Dorosz                                         *
 *                                                                             *
 * This file is part of Cryptizer.                                             *
 *                                                                             *
 *   Cryptizer is free software: you can redistribute it and/or modify it      *
 *   under the terms of the GNU Lesser General Public License as published     *
 *   by the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   Cryptizer is distributed in the hope that it will be useful,              *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU Lesser General Public License for more details.                       *
 *                                                                             *
 *   You should have received a copy of the GNU Lesser General Public          *
 *   License along with Cryptizer. If not, see <http://www.gnu.org/licenses/>. *
 *******************************************************************************/

/**
* @file crypt.h
* @author Damian Dorosz
* @date 25 Nov 2019
* @brief Cryptizer main header.
*
* General purpose of this is example how to cryptographic works.
*/

#ifndef _CRYPT_H
#define _CRYPT_H

#include <cstddef>
#include <thread>
#include <vector>
#include <cmath>

#include "io.h"

#define MAX_THREADS 32

/**
* @brief cryptDataType
*
* This is cryptic data type is equal to 8-bit value per one data index.
*/
typedef unsigned char *cryptDataType; // requirement data type for processing

using namespace std;

/**
* @brief cryptInfo
*
* Main cryptic structure designed to thread processing
*/
typedef struct _cryptinfo {
	size_t currentDataIndex;
	size_t currentDataSize;
	cryptDataType currentData;

	/** @brief cryptAlgorithm - this is special cryptic algorithm
	* required for encrypting/decrypting
	*
	* @return true - if is possible to encrypt/decrypt data in passed index or false if not
	*
	*/
	bool cryptAlgorithm(size_t index);

	//~_cryptinfo();
} cryptInfo;


/**
* @brief cryptWork
*
* Multi-threading main cryptic method
*/
static void cryptWork(void * workArgs); // could be in class

/**
*  @brief cryptic - crypt.h crypt.cpp
*  An example of the cryptographical class based on 8-bit processing.
*/
class cryptic {
public:
	/** @brief constructor cryptic - initializing required crypting data for processing purposes.
	*
	* @param cryptData cryptDataType/void* the data for cryptic-processing
	*                initialized for example by malloc(...)
	* @param cryptDataSize cryptDataType/void* the data size
	*                should represent of maximum possible data is stored in cryptData
	* @return void
	*
	*/
	cryptic(); // required for initialize nullable class entities! (use it only in special cases!)
	cryptic(void *cryptData, size_t cryptDataSize);
	cryptic(size_t cryptDataSize);
	//cryptic(cryptDataType cryptData, size_t cryptDataSize);

	~cryptic(); // required for free cryptic resources!

	// in this case encryption and decryption is the same so is no longer needed
	// cryptDataType encrypt();
	
	/** @brief decrypt - encryption/decryption processing for cryptic class
	*
	* @return cryptDataType data
	*
	*/
	cryptDataType decrypt(); // we are rude so is not compatible with old C

	/** @brief getCryptData - gets crypting data from cryptic class
	*
	* @return cryptDataType - data
	*
	*/
	cryptDataType getCryptData();

	/** @brief getCryptDataSize - gets crypting data size from cryptic class
	*
	* @return size_t - data size
	*
	*/
	size_t getCryptDataSize();

	/** @brief setCryptData - sets crypting data for cryptic class
	*
	* @param cryptData <Any type>* [optional] - the data for cryptic-processing
	*                initialized for example by malloc(...)
	* @param cryptDataSize size_t - the data size
	*                should represent of maximum possible data is stored in cryptData
	* @return void
	*
	*/
	//void setCryptData(cryptDataType cryptData, size_t cryptDataSize);
	template <class T>
	inline void setCryptData(T *cryptData, size_t cryptDataSize) {
		this->cryptData = (cryptDataType)cryptData;
		this->cryptDataSize = cryptDataSize;
	}
	void setCryptData(size_t cryptDataSize);

private:
	cryptDataType cryptData;		/**< cryptic private data pointer */
	size_t cryptDataSize;			/**< cryptic private data size */
	// TODO: separate it for thread class
	thread cryptThreads[MAX_THREADS];	/**< cryptic private threads */

	/** @brief cryptProcessing - main method purposes is processing 
	* all included cryptic data for encrypting/decrypting
	*
	* @param argValue intptr_t - decide whether value is index or size.
	* When argValue is equal to 0 then all data will be processing in once.
	* When argValue is above 0 then argValue is tract as start index for cryptic data.
	* When argValue is below 0 then argValue is tract as maximal data to processing per thread.
	*
	* @return void
	*
	*/
	void cryptProcessing(intptr_t argValue);
};

#endif /* _CRYPT_H */